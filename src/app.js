'use strict';

const express = require('express');
const path = require('path');
const app = express();
const bodyParser = require('body-parser');
const helmet = require('helmet');
const config = require(path.join(__dirname, '../config'));

const parse = require(path.join(__dirname, './parse/'));
const check = require(path.join(__dirname, './validate/'));

const PORT = config.MAIN.PORT;

app.use(bodyParser.json()); // for parsing application/json
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);

app.use(helmet());

const list = [];
const valid = [];

app.get('/', async (req, res) => {
  console.info('GET  ');
  if (req) {
    const {status, data} = await parse();
    res.statusCode(status).send(data);
  } else {
    res.statusCode(302).send([]);
  }
});

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}!`);
});
