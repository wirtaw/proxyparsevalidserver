'use strict';

const path = require('path');
const fs = require('fs');
const ProxyLists = require('proxy-lists');

const check = require(path.join(__dirname, './validate/'));

const config = require(path.join(__dirname, './../config'));

const options = {
  countries: config.PARSE.PARSE_COUNTRIES,
  sourcesWhiteList: config.PARSE.PARSE_WHITELIST,
  protocols: ['http', 'https'],
  anonymityLevels: ['elite'],
};
let list = [];
const valid = [];

(async () => {
  debugger;
  console.info('proxies start! ');

  const sources = ProxyLists.listSources();
  // console.info(`sources ${JSON.stringify(sources, null ,' ')}`);

  if (sources && Array.isArray(sources)) {
    debugger;
    const existsWhiteList = [];
    const sourcesNames = sources.map(item => item.name)
      .filter(item => !config.PARSE.PARSE_BLACKLIST.includes(item));

    /* options.sourcesWhiteList.forEach(source => {
      if (sourcesNames.inludes(source)) {
        existsWhiteList.push(source);
      }
    }); */
    options.sourcesWhiteList = [...sourcesNames];
  }

  console.info(`${JSON.stringify(options, null, ' ')}`);

  ProxyLists.getProxies(options)
    .on('data', (proxies) =>{
      debugger;
      list = list.concat(proxies);
      console.info('proxies data!');
    })
    .on('error', (error) => {
      console.error('error!', error);
    })
    .once('end', async () => {
      if (list && Array.isArray(list) && list.length > 0) {
        console.dir(list);
        const promises = [];
        const proxiesToCheck = [];

        list.forEach(proxy => {
          if (!proxiesToCheck.includes(proxy)) {
            proxiesToCheck.push(proxy);
            promises.push(check(proxy));
          }
        });

        await Promise.allSettled(promises).then(results => {
          if (results && Array.isArray(results) && results.length > 0) {
            console.info('Check finished successfully ');

            for (const item of results) {
              if (item.status && item.status === 'fulfilled' && item.value.answer) {
                valid.push(item.value.proxy);
              }
            }
          }
          console.dir(valid, {depth: 2});
          const dt = new Date();
          const dtPart = `${dt.getFullYear()}-${dt.getMonth() + 1}-${dt.getDate()}_${dt.getHours()}:${dt.getMinutes()}`;
          fs.promises.writeFile(path.join(__dirname, `./data/valid-proxy-${dtPart}.json`),
            JSON.stringify(valid),
            {encoding: 'utf8'});
        // console.dir(results);
        }).catch(err => {
          console.error('error!', err);

        // console.dir(results);
        });
      } else {
        console.error('Empty list');
      }
    });
})();
