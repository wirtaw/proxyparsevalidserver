'use strict';

const fs = require('fs');
const path = require('path');
const url = require('url');
const http = require('http');
const https = require('https');
// const puppeteer = require('puppeteer');
// const ProxyChain = require('proxy-chain');
// const got = require('got');
// const HttpProxyAgent = require('http-proxy-agent');
// const {proxyRequest} = require('puppeteer-proxy');

const axios = require('axios');
const httpsProxyAgent = require('https-proxy-agent');
const httpProxyAgent = require('http-proxy-agent');
const tunnel = require('tunnel');

const PROXY_PORT = 8081;
const ROUTER_PROXY = `http://127.0.0.1:${PROXY_PORT}`;

const FILENAME = '';

const proxies = {};

const dt = new Date();
const dtPart = `${dt.getFullYear()}-${dt.getMonth() + 1}-${dt.getDate()}_20:44`;
try {
  const data = fs.readFileSync(path.join(__dirname, `./data/valid-proxy-${dtPart}.json`), {encoding: 'utf8'});
  const proxiesList = JSON.parse(data);
  console.dir(proxiesList, {depth: 1});
} catch (e) {
  console.error(`Read or parse error ${e.meesage}`);
}

/* const server = new ProxyChain.Server({
  // Port where the server the server will listen. By default 8000.
  port: PROXY_PORT,

  prepareRequestFunction: ({request}) => {
    const userAgent = request.headers['user-agent'];
    const proxy = proxies[userAgent];
    return {
      upstreamProxyUrl: proxy,
    };
  }
});

server.listen(() => {
  console.log(`Router Proxy server is listening on port ${PROXY_PORT}`);
}); */

/* (async() => {
  const browser = await puppeteer.launch({
    args: [`--proxy-server=https://52.179.18.244:8080`],
  });
  const page = await browser.newPage();
  const partOne = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2)';
  const partTwo = 'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36';
  var userAgent = JSON.stringify({
    "user-agent" : `${partOne} ${partTwo}`,
    "proxy-addr" : "203.189.89.153",
    "proxy-port" : "8080",
  });
  // page.setUserAgent(userAgent);
  // await page.goto('https://razborkin.by');
  await page.goto('http://checkip.org/');
  await page.waitFor(1000);

  const ip = await page.$eval('span', el => el.innerHTML);
  console.info(`ip ${ip}`);

  await browser.close();
  //server.close();
})(); */

/* (async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();  // 1. Enable request/ response interception
  await page.setRequestInterception(true);  // 2. Intercept request
  page.once('request', async (request) => {
    const agent = new HttpProxyAgent('http://203.189.89.153:8080');
    console.info(`agent ${JSON.stringify(agent.proxy)} ${request.url()} `);
    const response = await got(request.url(), {
      // HTTP proxy.
      agent: {
        http: agent
      },
      body: request.postData(),
      headers: request.headers(),
      method: request.method(),
      retry: 0,
      throwHttpErrors: false,
    });    // 4. Return response to Chrome
    await request.respond({
      body: response.body,
      headers: response.headers,
      status: response.statusCode,
    });
  });

  await page.goto('http://checkip.org/');
  //await page.waitFor(1000);

  //const ip = await page.$$('span', el => el.innerHTML);
  //console.info(`ip ${ip}`);

  await browser.close();
})(); */

(async () => {
  const dt = new Date();
  const dtPart = `${dt.getFullYear()}-${dt.getMonth() + 1}-${dt.getDate()}_18:12`;
  try {
    const data = fs.readFileSync(path.join(__dirname, `./data/valid-proxy-${dtPart}.json`), {encoding: 'utf8'});
    const proxiesList = JSON.parse(data);
    console.dir(proxiesList, {depth: 1});
    let connectionTimeMin = 10000;
    let fastProxyUrl = '';
    let proxyHost = '';
    let proxyPort = '';
    const endpoint = 'http://checkip.org/';
    const poolSize = 10;

    proxiesList.forEach(proxy => {
      if (proxy['user-agent'] === true) {
        const protocol = (proxy.supportsHttps) ? 'https' : proxy.protocol;
        const proxyUrl = `${protocol}://${proxy.ip}:${proxy.port}`;
        proxies[proxy.ip] = proxyUrl;
        if (connectionTimeMin > proxy.connectionTime) {
          connectionTimeMin = proxy.connectionTime;
          fastProxyUrl = proxyUrl;
          proxyHost = `${proxy.ip}`;
          proxyPort = `${proxy.port}`;
        }
      }
    });
    const opts = url.parse(endpoint);
    let agent = null;
    const config = {
      method: 'GET',
      url: endpoint,
      proxy: false,
    };
    let tunnelingAgent = null;
    const axiosDefaultConfig = {
      baseURL: endpoint,
      proxy: {
        host: proxyHost,
        port: proxyPort,
        protocol: 'http',
      },
    };

    if (endpoint.indexOf('https:') > -1) {
      agent = new httpsProxyAgent(fastProxyUrl);
      config['httpsAgent'] = agent;
    } else {
      agent = new httpProxyAgent(fastProxyUrl);
      opts.agent = agent;
      config['proxy'] = {
        host: proxyHost,
        port: proxyPort,
        protocol: 'http',
      };
      tunnelingAgent = tunnel.httpOverHttp({
        maxSockets: poolSize, // Defaults to http.Agent.defaultMaxSockets

        proxy: { // Proxy settings
          host: proxyHost, // Defaults to 'localhost'
          port: proxyPort, // Defaults to 80
          // localAddress: localAddress, // Local interface if necessary

          // Basic authorization for proxy server if necessary
          // proxyAuth: 'user:password',

          // Header fields for proxy server if necessary
          headers: {
            'User-Agent': 'Node',
          },
        },
      });
    }

    console.dir(opts, {depth: 1});

    const resp = await axios.create(axiosDefaultConfig).request(config);
    console.dir(resp, {depth: 1});

    http.get(opts, function(res) {
      console.log('opts ', opts);
      console.log('"response" event!', res.headers);
      res.pipe(process.stdout);
    });

    const req = http.request({
      host: endpoint,
      port: 80,
      agent: tunnelingAgent,
    });
    console.dir(req, {depth: 1});
  } catch (e) {
    console.error(`Read or parse error ${e}`);
  }
})();
