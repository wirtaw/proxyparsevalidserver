'use strict';

const path = require('path');
const checkProxy = require('check-proxy').check;

const config = require(path.join(__dirname, './../../config'));

const check = async (item) => {
  return new Promise((resolve, reject) => {
    if (item) {
      const ip = item.ipAddress || '';
      const port = item.port || '';

      checkProxy({
        testHost: config.VALIDATOR.TEST_HOST, // put your ping server url here
        proxyIP: ip, // proxy ip to test
        proxyPort: port, // proxy port to test
        localIP: config.VALIDATOR.IP, // local machine IP address to test
        connectTimeout: config.VALIDATOR.CONNECT_TIMEOUT, // curl connect timeout, sec
        timeout: config.VALIDATOR.TIMEOUT, // curl timeout, sec
        websites: [
          {
            name: 'example',
            url: 'http://www.example.com/',
            regex: /example/gim, // expected result - regex
          },
          {
            name: 'google',
            url: 'http://www.google.com/',
            regex: function(html) { // expected result - custom function
              return html && html.indexOf('google') !== -1;
            },
          },
          {
            name: 'amazon',
            url: 'http://www.amazon.com/',
            regex: 'Amazon', // expected result - look for this string in the output
          },

        ],
      }).then((result) => {
        // console.log('final result', result);
        // console.dir(result[0], {depth: 2});
        resolve(Object.assign({answer: true}, {proxy: result[0]}));
      }, (err) => {
        // console.log('proxy rejected', err);
        reject(Object.assign({answer: false}, {reason: err}));
      }).catch(e => {
        // console.log('checkProxy rejected', e.toString());
        reject(Object.assign({answer: false}, {reason: e}));
      });
    } else {
      reject(Object.assign({answer: false}, {emptyProxy: true}));
    }
  });
};

module.exports = check;
