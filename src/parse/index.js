'use strict';

const path = require('path');
const ProxyLists = require('proxy-lists');

const config = require(path.join(__dirname, './../../config'));

const options = {
  countries: config.PARSE.PARSE_COUNTRIES,
  sourcesWhiteList: config.PARSE.PARSE_WHITELIST,
  protocols: ['http', 'https'],
  anonymityLevels: ['elite'],
};

let list = [];
const valid = [];
const check = [];

const parse = () => {
  return ProxyLists.getProxies(options).on('data', (proxies) =>{
    debugger;
    list = list.concat(proxies);
  })
    .on('error', (error) => {
      debugger;
      console.error('error!', error);
    })
    .once('end', async () => {
      if (list && Array.isArray(list) && list.length > 0) {
        const promises = [];

        list.forEach(proxy => {
          promises.push(check(proxy));
        });

        await Promise.allSettled(promises).then(results => {
          if (results && Array.isArray(results) && results.length > 0) {
            console.info('Check finished successfully ');

            for (const item of results) {
              if (item.status && item.status === 'fulfilled' && item.value.answer) {
                valid.push(item.value.proxy);
              }
            }
          }
          console.dir(valid, {depth: 2});
        // console.dir(results);
        }).catch(err => {
          console.error('error!', err);

        // console.dir(results);
        });
      } else {
        console.error('Empty list');
      }
    });
};

module.exports = parse;
