'use strict';

const path = require('path');
const fs = require('fs');
const {Worker} = require('worker_threads');

function runService(workerData) {
  return new Promise((resolve, reject) => {
    const worker = new Worker(
      './src/worker.js', {workerData},
    );
    worker.on('message', resolve);
    worker.on('error', reject);
    worker.on('exit', (code) => {
      if (code !== 0)
        reject(new Error(
          `Stopped the Worker Thread with the exit code: ${code}`,
        ));
    });
  });
}

(async () => {
  console.info('proxies start! ');
  const fileName = 'valid-proxy-2022-3-9_19:35';
  try {
    const enabledData = await fs.promises.readFile(path.join(__dirname, './data/items.txt'), {encoding: 'utf8'});
    const proxiesData = await fs.promises.readFile(path.join(__dirname, `./data/${fileName}.json`), {encoding: 'utf8'});

    const enabled = JSON.parse(enabledData);
    const proxies = JSON.parse(proxiesData);

    // console.dir(enabled);
    // console.dir(proxies);
    for await (const proxy of proxies) {
      for await (const item of enabled) {
        const {url} = item;
        const {ip, port, protocol} = proxy;
        const res = await runService({url, proxy: `${protocol}://${ip}:${port}`});
        console.info(`statusCode ${res.statusCode} ${res.body}`);
      }
    }
  } catch (err) {
    console.error(err);
  }
})();
