'use strict';

let sourcesWhiteList = [];
let sourcesBlackList = [];
let countries = [];

if (process.env.PARSE_WHITELIST) {
  if (process.env.PARSE_WHITELIST.indexOf(',') > -1) {
    sourcesWhiteList = sourcesWhiteList.concat(process.env.PARSE_WHITELIST.split(','));
  } else {
    sourcesWhiteList.push(process.env.PARSE_WHITELIST);
  }
}

if (process.env.PARSE_BLACKLIST) {
  if (process.env.PARSE_BLACKLIST.indexOf(',') > -1) {
    sourcesBlackList = sourcesBlackList.concat(process.env.PARSE_BLACKLIST.split(','));
  } else {
    sourcesBlackList.push(process.env.PARSE_BLACKLIST);
  }
}

if (process.env.PARSE_COUNTRIES) {
  if (process.env.PARSE_COUNTRIES.indexOf(',') > -1) {
    countries = countries.concat(process.env.PARSE_COUNTRIES.split(','));
  } else {
    countries.push(process.env.PARSE_COUNTRIES);
  }
}

module.exports = {
  MAIN: {
    PORT: Number(process.env.PORT) || 8081,
  },
  VALIDATOR: {
    TEST_HOST: process.env.TEST_HOST || 5000,
    IP: process.env.IP || '127.0.0.1',
    CONNECT_TIMEOUT: Number(process.env.CONNECT_TIMEOUT) || 10,
    TIMEOUT: Number(process.env.TIMEOUT) || 5,
  },
  PARSE: {
    PARSE_WHITELIST: sourcesWhiteList,
    PARSE_BLACKLIST: sourcesBlackList,
    PARSE_COUNTRIES: countries,
  },
};
